# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    area = fields.Float(
        compute='_get_area',
    )

    @api.depends('product_id', 'inventory_quantity')
    def _get_area(self):
        for quant in self:
            quant.area = quant.product_id.width * quant.inventory_quantity
