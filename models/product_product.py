# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ProductProduct(models.Model):
    _inherit = "product.product"

    width = fields.Float(
        compute="_get_width",
        store=True,
    )

    @api.depends("product_template_attribute_value_ids")
    def _get_width(self):
        product_attribute_width_id = self.env.ref(
            "uom_equivalence.product_attribute_width",
            raise_if_not_found=False,
        )
        if not product_attribute_width_id:
            return
        for product in self:
            attribute_value_id = product.product_template_attribute_value_ids.filtered(
                lambda line: line.attribute_id == product_attribute_width_id
            )
            product.width = attribute_value_id and float(attribute_value_id[0].name) or 0
