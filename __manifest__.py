# -*- coding: utf-8 -*-
{
    'name': 'UoM Equivalence',
    'version': '13.0.0.1.0.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'stock',
        'product',
    ],
    'data': [
        # security
        # data
        'data/product_attribute.xml',
        # reports
        # views
        'views/stock_quant.xml',
    ],
}
